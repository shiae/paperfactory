﻿using ISP31ShirkunovPaperFactory.models;
using ISP31ShirkunovPaperFactory.Windows;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Type = ISP31ShirkunovPaperFactory.models.Type;

namespace ISP31ShirkunovPaperFactory
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PaperFactoryDbContext paperFactoryDbContext;
        private ObservableCollection<Material> _materials;
        private ObservableCollection<Type> _types;
        private int itemsPerPage;

        public ObservableCollection<Material> Materials
        {
            get => _materials;
            set
            {
                _materials = value;
                MaterialsCollectionView = CollectionViewSource.GetDefaultView(value);
                MaterialsCollectionView.Filter = FilterItems;
                MaterialsCollectionView.CollectionChanged += MaterialsCollectionView_CollectionChanged;
            }
        }

        public ObservableCollection<Type> Types
        {
            get => _types;
            set => _types = value;
        }

        public ICollectionView MaterialsCollectionView { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            paperFactoryDbContext = new PaperFactoryDbContext();
            Materials = new ObservableCollection<Material>(paperFactoryDbContext.Materials.ToList());
            Types = new ObservableCollection<Type>(paperFactoryDbContext.Types.ToList().Prepend(new Type { name_type = "Все типы"}));
            DataContext = this;
            itemsPerPage = 15;
            ChangeNumberOfRecords();
            PaintItemsForCurrentPage();
        }

        private bool FilterItems(object obj)
        {
            Material material = obj as Material;
            bool result = true;

            if (material != null)
            {
                if (!string.IsNullOrWhiteSpace(tbInput.Text))
                {
                    if (!string.IsNullOrEmpty(material.name_material))
                    {
                        result = material.name_material.ToLower().Contains(tbInput.Text.ToLower());
                    }

                    if (!string.IsNullOrEmpty(material.opisanie))
                    {
                        result = result || material.opisanie.ToLower().Contains(tbInput.Text.ToLower());
                    }
                }

                if (cbFilter.SelectedItem != null && (cbFilter.SelectedItem as Type).name_type != "Все типы")
                {
                    result = result && (cbFilter.SelectedItem as Type).id_type == material.id_type;
                }
            }

            return result;
        }

        private void MaterialsCollectionView_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ChangeNumberOfRecords();
        }

        private void ChangeNumberOfRecords()
        {
            tbNumberOfRecords.Text = $"{MaterialsCollectionView.Cast<object>().Count()} из {Materials.Count}";
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdatePages();
        }

        private void cbFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdatePages();
        }

        private void UpdatePages()
        {
            MaterialsCollectionView.Refresh();
            lbPages.ItemsSource = Enumerable.Range(1, (int)Math.Ceiling((double)MaterialsCollectionView.Cast<object>().Count() / (double)itemsPerPage));
            ChangeNumberOfRecords();
            PaintItemsForCurrentPage();
        }

        private void cbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MaterialsCollectionView != null)
            {
                MaterialsCollectionView.SortDescriptions.Clear();
                ListSortDirection listSortDescription = cbSort.SelectedIndex == 0 ? ListSortDirection.Descending : ListSortDirection.Ascending;
                MaterialsCollectionView.SortDescriptions.Add(new SortDescription("name_material", listSortDescription));
                MaterialsCollectionView.SortDescriptions.Add(new SortDescription("kol_na_sklade", listSortDescription));
                MaterialsCollectionView.SortDescriptions.Add(new SortDescription("cena", listSortDescription));
                MaterialsCollectionView.Refresh();
                PaintItemsForCurrentPage();
            }
        }

        private void lbPages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PaintItemsForCurrentPage();
        }

        private void PaintItemsForCurrentPage()
        {
            var allItems = MaterialsCollectionView.Cast<object>();
            int currentPage = (int)(lbPages.SelectedItem ?? 1);
            var itemsForCurrentPage = allItems.Skip((currentPage - 1) * itemsPerPage).Take(itemsPerPage);
            lvMaterials.ItemsSource = itemsForCurrentPage;
        }

        private void lvMaterials_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvMaterials.SelectedItems.Count > 1)
            {
                btnChangeMinimumQuantity.Visibility = Visibility.Visible;
            }
            else
            {
                btnChangeMinimumQuantity.Visibility = Visibility.Collapsed;
            }
        }

        private void btnChangeMinimumQuantity_Click(object sender, RoutedEventArgs e)
        {
            var inputWindow = new InputWindow();

            if (inputWindow.ShowDialog() == true)
            {
                int newMinQuantity = inputWindow.NewMinQuantity;

                foreach (Material selectedMaterial in lvMaterials.SelectedItems)
                {
                    selectedMaterial.ostatok = newMinQuantity;
                    paperFactoryDbContext.SaveChanges();
                }
            }

            PaintItemsForCurrentPage();
        }

        private void lvMaterials_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Material material = lvMaterials.SelectedItem as Material;

            if (material != null)
            {
                MaterialWindow materialWindow = new MaterialWindow(material, paperFactoryDbContext, true);
                materialWindow.ShowDialog();
                Materials = new ObservableCollection<Material>(paperFactoryDbContext.Materials.ToList());
                UpdatePages();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MaterialWindow materialWindow = new MaterialWindow(new Material(), paperFactoryDbContext);
            materialWindow.ShowDialog();
            Materials = new ObservableCollection<Material>(paperFactoryDbContext.Materials.ToList());
            UpdatePages();
        }
    }
}
