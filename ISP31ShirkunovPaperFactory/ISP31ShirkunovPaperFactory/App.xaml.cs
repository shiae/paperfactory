﻿using ISP31ShirkunovPaperFactory.models;
using System.Threading.Tasks;
using System;
using System.Windows;
using System.Windows.Threading;

namespace ISP31ShirkunovPaperFactory
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Настраиваем главное окно приложения
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowGlobalErrorMessage(((Exception)e.ExceptionObject).Message);
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            ShowGlobalErrorMessage(e.Exception.Message);
        }

        /// <summary>
        /// Используется когда происходит ошибка в асинхронном коде
        /// </summary>
        private void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            ShowGlobalErrorMessage(e.Exception.Message);
        }

        private void ShowGlobalErrorMessage(string message)
        {
            MessageBox.Show($"Не волнуйтесь! Произошла непредвиденная ошибка приложения: {message}. Обратитесь к системному администратору!", "Непредвиденная ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
