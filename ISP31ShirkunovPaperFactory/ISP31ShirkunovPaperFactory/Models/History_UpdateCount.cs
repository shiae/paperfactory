namespace ISP31ShirkunovPaperFactory.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class History_UpdateCount
    {
        [Key]
        public int id_historyupdate { get; set; }

        [StringLength(50)]
        public string name_material { get; set; }

        public decimal? count_old { get; set; }

        public decimal? count_new { get; set; }
    }
}
