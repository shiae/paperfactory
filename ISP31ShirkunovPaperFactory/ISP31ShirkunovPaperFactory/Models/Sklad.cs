namespace ISP31ShirkunovPaperFactory.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sklad")]
    public partial class Sklad
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_material { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_postavshik { get; set; }

        [Column(TypeName = "money")]
        public decimal? stoimost_ed { get; set; }

        public virtual Material Material { get; set; }

        public virtual Postavshik Postavshik { get; set; }
    }
}
