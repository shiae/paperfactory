namespace ISP31ShirkunovPaperFactory.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Postavshik")]
    public partial class Postavshik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Postavshik()
        {
            Sklads = new HashSet<Sklad>();
        }

        [Key]
        public int id_postavshik { get; set; }

        [StringLength(50)]
        public string name_postavshik { get; set; }

        [StringLength(80)]
        public string email_postavshik { get; set; }

        [StringLength(30)]
        public string phone_postavshik { get; set; }

        [Column(TypeName = "text")]
        public string logo_postavshik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sklad> Sklads { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;

            Postavshik postavshik = obj as Postavshik;
            return id_postavshik == postavshik.id_postavshik;
        }
    }
}
