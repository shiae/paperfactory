namespace ISP31ShirkunovPaperFactory.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sotrudnik")]
    public partial class Sotrudnik
    {
        [Key]
        public int id_sotrudnik { get; set; }

        public string FIO { get; set; }

        public int id_proizvodstvo { get; set; }

        public virtual Proizvodstvo Proizvodstvo { get; set; }
    }
}
