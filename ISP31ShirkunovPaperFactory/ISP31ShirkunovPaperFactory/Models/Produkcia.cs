namespace ISP31ShirkunovPaperFactory.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Produkcia")]
    public partial class Produkcia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Produkcia()
        {
            Material_Produkcia = new HashSet<Material_Produkcia>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_produkcia { get; set; }

        [StringLength(10)]
        public string name_produkcia { get; set; }

        public int id_proizvodstvo { get; set; }

        public int? id_type_produkcii { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Material_Produkcia> Material_Produkcia { get; set; }

        public virtual Proizvodstvo Proizvodstvo { get; set; }

        public virtual Type_Produkcii Type_Produkcii { get; set; }
    }
}
