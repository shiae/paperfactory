using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace ISP31ShirkunovPaperFactory.models
{
    public partial class PaperFactoryDbContext : DbContext
    {
        public PaperFactoryDbContext()
            : base("name=ConnectionString")
        {
        }

        public virtual DbSet<History_UpdateCount> History_UpdateCount { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<Material_Produkcia> Material_Produkcia { get; set; }
        public virtual DbSet<Postavshik> Postavshiks { get; set; }
        public virtual DbSet<Produkcia> Produkcias { get; set; }
        public virtual DbSet<Proizvodstvo> Proizvodstvoes { get; set; }
        public virtual DbSet<Sklad> Sklads { get; set; }
        public virtual DbSet<Sotrudnik> Sotrudniks { get; set; }
        public virtual DbSet<Type> Types { get; set; }
        public virtual DbSet<Type_Produkcii> Type_Produkcii { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<History_UpdateCount>()
                .Property(e => e.name_material)
                .IsUnicode(false);

            modelBuilder.Entity<Material>()
                .Property(e => e.name_material)
                .IsUnicode(false);

            modelBuilder.Entity<Material>()
                .Property(e => e.image)
                .IsUnicode(false);

            modelBuilder.Entity<Material>()
                .Property(e => e.cena)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Material>()
                .Property(e => e.opisanie)
                .IsUnicode(false);

            modelBuilder.Entity<Material>()
                .Property(e => e.ed_ismer)
                .IsUnicode(false);

            modelBuilder.Entity<Material>()
                .HasMany(e => e.Material_Produkcia)
                .WithRequired(e => e.Material)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Material>()
                .HasMany(e => e.Sklads)
                .WithRequired(e => e.Material)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Postavshik>()
                .Property(e => e.name_postavshik)
                .IsUnicode(false);

            modelBuilder.Entity<Postavshik>()
                .Property(e => e.email_postavshik)
                .IsUnicode(false);

            modelBuilder.Entity<Postavshik>()
                .Property(e => e.phone_postavshik)
                .IsUnicode(false);

            modelBuilder.Entity<Postavshik>()
                .Property(e => e.logo_postavshik)
                .IsUnicode(false);

            modelBuilder.Entity<Postavshik>()
                .HasMany(e => e.Sklads)
                .WithRequired(e => e.Postavshik)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produkcia>()
                .Property(e => e.name_produkcia)
                .IsFixedLength();

            modelBuilder.Entity<Produkcia>()
                .HasMany(e => e.Material_Produkcia)
                .WithRequired(e => e.Produkcia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proizvodstvo>()
                .Property(e => e.name_proizvodstvo)
                .IsUnicode(false);

            modelBuilder.Entity<Proizvodstvo>()
                .HasMany(e => e.Produkcias)
                .WithRequired(e => e.Proizvodstvo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proizvodstvo>()
                .HasMany(e => e.Sotrudniks)
                .WithRequired(e => e.Proizvodstvo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sklad>()
                .Property(e => e.stoimost_ed)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Sotrudnik>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<Type>()
                .Property(e => e.name_type)
                .IsUnicode(false);

            modelBuilder.Entity<Type_Produkcii>()
                .Property(e => e.type_produkcii1)
                .IsUnicode(false);
        }
    }
}
