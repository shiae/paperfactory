namespace ISP31ShirkunovPaperFactory.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.IO;
    using System.Reflection;
    using System.Windows.Media.Imaging;

    [Table("Material")]
    public partial class Material
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Material()
        {
            Material_Produkcia = new HashSet<Material_Produkcia>();
            Sklads = new HashSet<Sklad>();
        }

        [Key]
        public int id_material { get; set; }

        [StringLength(50)]
        public string name_material { get; set; }

        [Column(TypeName = "text")]
        public string image { get; set; }

        public decimal? ostatok { get; set; }

        [Column(TypeName = "money")]
        public decimal? cena { get; set; }

        public int? id_type { get; set; }

        public string opisanie { get; set; }

        [StringLength(10)]
        public string ed_ismer { get; set; }

        public decimal? kol_na_sklade { get; set; }

        public decimal? kol_v_upakovke { get; set; }

        [NotMapped]
        public string Background
        {
            get
            {
                if (kol_na_sklade < ostatok)
                {
                    return "#f19292";
                }
                else if (kol_na_sklade == ostatok * 3)
                {
                    return "#ffba01";
                }
                else
                {
                    return "#fff";
                }
            }
        }

        [NotMapped]
        public BitmapImage BitmapImage
        {
            get
            {
                string directoryPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\";
                string imagePath = string.IsNullOrEmpty(image) ? @"\image\default.jpg" : image;

                if (!File.Exists(directoryPath + imagePath))
                {
                    imagePath = @"\image\default.jpg";
                }

                return new BitmapImage(new Uri(directoryPath + imagePath));
            }
            
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Material_Produkcia> Material_Produkcia { get; set; }

        public virtual Type Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sklad> Sklads { get; set; }
    }
}
