﻿using ISP31ShirkunovPaperFactory.models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ISP31ShirkunovPaperFactory.Windows
{
    /// <summary>
    /// Логика взаимодействия для MaterialWindow.xaml
    /// </summary>
    public partial class MaterialWindow : Window, INotifyPropertyChanged
    {
        private string nameMaterial;
        private ObservableCollection<models.Type> types;
        private models.Type type;
        private decimal? quantity;
        private string measureUnit;
        private decimal? quantityPack;
        private decimal? minQuantity;
        private decimal? cost;
        private string description;
        private BitmapImage image;
        private ObservableCollection<Postavshik> postavshiks;
        private ObservableCollection<Postavshik> materialPostavshiks;
        private decimal? minCost;

        public event PropertyChangedEventHandler PropertyChanged;

        public string NameMaterial
        {
            get => nameMaterial;
            set
            {
                nameMaterial = value;
                OnPropertyChanged();
            }
        }

        public decimal? MinCost
        {
            get => minCost;
            set
            {
                minCost = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<models.Type> Types
        {
            get => types;
            set
            {
                types = value;
                OnPropertyChanged();
            }
        }

        public models.Type Type
        {
            get => type;
            set
            {
                type = value;
                OnPropertyChanged();
            }
        }

        public decimal? Quantity
        {
            get => quantity;
            set
            {
                quantity = value;
                ChangeMinCost();
                OnPropertyChanged();
            }
        }

        public string MeasureUnit
        {
            get => measureUnit;
            set
            {
                measureUnit = value;
                OnPropertyChanged();
            }
        }

        public decimal? QuantityPack
        {
            get => quantityPack;
            set
            {
                quantityPack = value;
                ChangeMinCost();
                OnPropertyChanged();
            }
        }
        
        public decimal? MinQuantity
        {
            get => minQuantity;
            set
            {
                minQuantity = value;
                ChangeMinCost();
                OnPropertyChanged();
            }
        }

        public decimal? Cost
        {
            get => cost;
            set
            {
                cost = value;
                ChangeMinCost();
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => description;
            set
            {
                description = value;
                OnPropertyChanged();
            }
        }

        public Material Material { get; set; }

        public PaperFactoryDbContext PaperFactoryDbContext { get; set; }

        public bool IsEditing { get; set; }

        public BitmapImage Image
        {
            get => image;
            set
            {
                image = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Postavshik> Postavshiks
        {
            get => postavshiks;
            set
            {
                postavshiks = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Postavshik> MaterialPostavshiks
        {
            get => materialPostavshiks;
            set
            {
                materialPostavshiks = value;
                OnPropertyChanged();
            }
        }

        public MaterialWindow(Material material, PaperFactoryDbContext paperFactoryDbContext, bool isEditing = false)
        {
            InitializeComponent();
            DataContext = this;
            PaperFactoryDbContext = paperFactoryDbContext;
            NameMaterial = material.name_material;
            Types = new ObservableCollection<models.Type>(PaperFactoryDbContext.Types.ToList());
            Type = material.Type;
            Quantity = material.kol_na_sklade;
            MeasureUnit = material.ed_ismer;
            QuantityPack = material.kol_v_upakovke;
            MinQuantity = material.ostatok;
            Cost = material.cena;
            Description = material.opisanie;
            Material = material;
            IsEditing = isEditing;
            Image = material.BitmapImage;
            Postavshiks = new ObservableCollection<Postavshik>(PaperFactoryDbContext.Postavshiks.ToList());
            MaterialPostavshiks = new ObservableCollection<Postavshik>(Material.Sklads.Select(sklad => sklad.Postavshik));
            ChangeMinCost();
        }

        private void ChangeMinCost()
        {
            if (Quantity < MinQuantity)
            {
                MinCost = Math.Ceiling((decimal)((MinQuantity - Quantity) / QuantityPack)) * QuantityPack * Cost;
            }
        }

        public void OnPropertyChanged([CallerMemberName] string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string erros = CheckErrors();

            if (!string.IsNullOrEmpty(erros))
            {
                MessageBox.Show(
                    erros,
                    "Ошибки",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return;
            }

            MessageBoxResult result = MessageBox.Show(
                "Вы уверены, что хотите сохранить?",
                "Сохранение данных",
                MessageBoxButton.YesNo,
                MessageBoxImage.Information
            );

            if (result == MessageBoxResult.Yes)
            {
                if (IsEditing)
                {
                    Material foundMaterial = PaperFactoryDbContext.Materials.Find(Material.id_material);
                    foundMaterial.name_material = NameMaterial;
                    foundMaterial.Type = Type;
                    foundMaterial.kol_na_sklade = Quantity;
                    foundMaterial.ed_ismer = MeasureUnit;
                    foundMaterial.kol_v_upakovke = QuantityPack;
                    foundMaterial.ostatok = MinQuantity;
                    foundMaterial.cena = Cost;
                    foundMaterial.opisanie = Description;
                    AddSklads();
                }
                else
                {
                    Material.name_material = NameMaterial;
                    Material.Type = Type;
                    Material.kol_na_sklade = Quantity;
                    Material.ed_ismer = MeasureUnit;
                    Material.kol_v_upakovke = QuantityPack;
                    Material.ostatok = MinQuantity;
                    Material.cena = Cost;
                    Material.opisanie = Description;
                    AddSklads();
                    PaperFactoryDbContext.Materials.Add(Material);
                }

                PaperFactoryDbContext.SaveChanges();
                DialogResult = true;
                this.Close();
            }
        }

        private string CheckErrors()
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (string.IsNullOrEmpty(NameMaterial))
            {
                stringBuilder.AppendLine("Укажите наименование материала!");
            }

            if (cbTypes.SelectedItem == null)
            {
                stringBuilder.AppendLine("Выберите тип материала!");
            }

            if (Quantity <= 0)
            {
                stringBuilder.AppendLine("Укажите количество больше 0!");
            }

            if (string.IsNullOrEmpty(MeasureUnit))
            {
                stringBuilder.AppendLine("Укажите единицу измерения!");
            }

            if (QuantityPack <= 0)
            {
                stringBuilder.AppendLine("Укажите количество в упакове больше 0!");
            }

            if (MinQuantity <= 0)
            {
                stringBuilder.AppendLine("Укажите минимальное количество больше 0!");
            }

            if (Cost <= 0)
            {
                stringBuilder.AppendLine("Укажите стоимость больше 0!");
            }

            if (MaterialPostavshiks.Count == 0)
            {
                stringBuilder.AppendLine("Добавьте хотя бы одного поставщика материала!");
            }

            return stringBuilder.ToString();
        }

        private void AddSklads()
        {
            ICollection<Sklad> sklads = new List<Sklad>();

            if (Material.Sklads != null)
            {
                sklads = Material.Sklads;
            }

            foreach (Postavshik postavshik in MaterialPostavshiks)
            {
                if (!sklads.Any(sklad => sklad.id_material == Material.id_material && sklad.id_postavshik == postavshik.id_postavshik))
                {
                    sklads.Add(new Sklad() { id_material = Material.id_material, id_postavshik = postavshik.id_postavshik, stoimost_ed = Material.cena });
                }
            }

            Material.Sklads = sklads;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(
                "Есть несохраненные данные, вы уверены, что хотите закрыть? Изменения не будут сохранены",
                "Закрытие окна",
                MessageBoxButton.YesNo,
                MessageBoxImage.Information
            );

            if (result == MessageBoxResult.Yes)
            {
                DialogResult = false;
                this.Close();
            }
        }

        private void btnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.png;*.jpeg;*.jpg;*.gif)|*.png;*.jpeg;*.jpg;*.gif";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var bitmap = new BitmapImage(new Uri(openFileDialog.FileName));

                    string filename = Path.GetFileName(openFileDialog.FileName);
                    string folderPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\image\";

                    Directory.CreateDirectory(folderPath);

                    using (var fileStream = new FileStream(folderPath + filename, FileMode.Create))
                    {
                        BitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(bitmap));
                        encoder.Save(fileStream);
                    }

                    Image = bitmap;
                    Material.image = @"\image\" + Path.GetFileName(openFileDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ошибка при выборе изображения: {ex.Message}");
                }
            }
        }

        private void btAddSupplier_Click(object sender, RoutedEventArgs e)
        {
            if (lbSuppliers.SelectedItems.Count > 0)
            {
                foreach (Postavshik postavshik in lbSuppliers.SelectedItems)
                {
                    if (!MaterialPostavshiks.Any(item => item.id_postavshik == postavshik.id_postavshik))
                    {
                        MaterialPostavshiks.Add(postavshik);
                    }
                }
            }
        }

        private void btDeleteSupplier_Click(object sender, RoutedEventArgs e)
        {
            if (lbMaterialSuppliers.SelectedItems.Count > 0)
            {
                var selectedItems = lbMaterialSuppliers.SelectedItems.Cast<object>().ToList();

                foreach (Postavshik postavshik in selectedItems)
                {
                    MaterialPostavshiks.Remove(postavshik);
                    Sklad skald = PaperFactoryDbContext.Sklads.Find(Material.id_material, postavshik.id_postavshik);

                    if (skald != null)
                    {
                        PaperFactoryDbContext.Sklads.Remove(skald);
                        PaperFactoryDbContext.SaveChanges();
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(
                "Вы точно хотите удалить?",
                "Удаление данных",
                MessageBoxButton.YesNo,
                MessageBoxImage.Information
            );

            if (result == MessageBoxResult.Yes)
            {
                if (Material.Material_Produkcia.Count == 0)
                {
                    if (Material.Sklads != null)
                    {
                        PaperFactoryDbContext.Sklads.RemoveRange(PaperFactoryDbContext.Sklads.Where(x => x.id_material == Material.id_material));
                    }

                    if (PaperFactoryDbContext.History_UpdateCount.Any(x => x.name_material == Material.name_material))
                    {
                        PaperFactoryDbContext.History_UpdateCount.RemoveRange(PaperFactoryDbContext.History_UpdateCount.Where(x => x.name_material == Material.name_material));
                    }

                    PaperFactoryDbContext.Materials.Remove(Material);
                    PaperFactoryDbContext.SaveChanges();
                    DialogResult = true;
                    this.Close();
                }
            }
        }
    }
}
