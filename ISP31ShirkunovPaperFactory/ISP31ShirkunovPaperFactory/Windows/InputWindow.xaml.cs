﻿using System.Windows;

namespace ISP31ShirkunovPaperFactory.Windows
{
    /// <summary>
    /// Логика взаимодействия для InputWindow.xaml
    /// </summary>
    public partial class InputWindow : Window
    {
        public int NewMinQuantity { get; set; }

        public InputWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int minQuantity;

            if (!int.TryParse(txtMinQuantity.Text, out minQuantity))
            {
                MessageBox.Show("Вы ввели недопустимое значение!");
                return;
            }

            NewMinQuantity = minQuantity;
            MessageBoxResult messageBoxResult = MessageBox.Show("Вы уверены, что хотите изменить?", "Изменить минимальное количество", MessageBoxButton.YesNo, MessageBoxImage.Information);
            DialogResult = messageBoxResult == MessageBoxResult.Yes;
        }
    }
}
