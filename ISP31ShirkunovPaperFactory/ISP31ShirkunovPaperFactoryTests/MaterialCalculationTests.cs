﻿using ISP31ShirkunovPaperFactoryLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ISP31ShirkunovPaperFactoryTests
{
    [TestClass]
    public class MaterialCalculationTests
    {
        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithValidInput_ShouldReturnCorrectQuantity()
        {
            int itemCount = 5;
            int itemWidth = 10;
            int itemLength = 20;
            int itemType = 2;
            int materialType = 1;
            long expectedQuantity = 2508;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithZeroItemCount_ShouldReturnNegativeOne()
        {
            int itemCount = 0;
            int itemWidth = 10;
            int itemLength = 20;
            int itemType = 2;
            int materialType = 1;
            long expectedQuantity = -1;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithNegativeItemWidth_ShouldReturnNegativeOne()
        {
            int itemCount = 5;
            int itemWidth = -10;
            int itemLength = 20;
            int itemType = 2;
            int materialType = 1;
            long expectedQuantity = -1;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithNegativeItemLength_ShouldReturnNegativeOne()
        {
            int itemCount = 5;
            int itemWidth = 10;
            int itemLength = -20;
            int itemType = 2;
            int materialType = 1;
            long expectedQuantity = -1;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithInvalidItemType_ShouldReturnNegativeOne()
        {
            int itemCount = 5;
            int itemWidth = 10;
            int itemLength = 20;
            int itemType = 4;
            int materialType = 1;
            long expectedQuantity = -1003;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithLargeItemCount_ShouldReturnCorrectQuantity()
        {
            int itemCount = 1000;
            int itemWidth = 5;
            int itemLength = 10;
            int itemType = 3;
            int materialType = 2;
            long expectedQuantity = 422007;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithLargeItemWidthAndLength_ShouldReturnCorrectQuantity()
        {
            int itemCount = 10;
            int itemWidth = 1000;
            int itemLength = 2000;
            int itemType = 1;
            int materialType = 1;
            long expectedQuantity = 22066199;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithMinimumValidInput_ShouldReturnCorrectQuantity()
        {
            int itemCount = 1;
            int itemWidth = 1;
            int itemLength = 1;
            int itemType = 1;
            int materialType = 1;
            long expectedQuantity = 2;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithMaximumValidInput_ShouldReturnCorrectQuantity()
        {
            int itemCount = short.MaxValue;
            int itemWidth = short.MaxValue;
            int itemLength = short.MaxValue;
            int itemType = 3;
            int materialType = 2;
            long expectedQuantity = 296933422714076;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }

        [TestMethod]
        public void CalculateRequiredMaterialQuantity_WithMaximumValidInputAndWithInvalidItemType_ShouldReturnCorrectQuantity()
        {
            int itemCount = short.MaxValue;
            int itemWidth = short.MaxValue;
            int itemLength = short.MaxValue;
            int itemType = 5;
            int materialType = 2;
            long expectedQuantity = -35223419064540;

            long actualQuantity = MaterialCalculation.CalculateRequiredMaterialQuantity(itemCount, itemWidth, itemLength, itemType, materialType);

            Assert.AreEqual(expectedQuantity, actualQuantity);
        }
    }
}
