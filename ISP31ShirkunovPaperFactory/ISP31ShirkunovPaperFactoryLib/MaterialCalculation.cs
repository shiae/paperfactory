﻿using System;

namespace ISP31ShirkunovPaperFactoryLib
{
    public class MaterialCalculation
    {
        public static long CalculateRequiredMaterialQuantity(int itemCount, int itemWidth, int itemLength, int itemType, int materialType)
        {
            if (IsInvalidInput(itemCount, itemWidth, itemLength))
            {
                return -1;
            }

            decimal itemTypeFactor = GetItemTypeFactor(itemType);
            decimal materialTypeFactor = GetMaterialTypeFactor(materialType);

            return CalculateQuantity(itemCount, itemWidth, itemLength, itemTypeFactor, materialTypeFactor);
        }

        private static bool IsInvalidInput(int itemCount, int itemWidth, int itemLength)
        {
            return itemCount <= 0 || itemWidth <= 0 || itemLength <= 0;
        }

        private static decimal GetItemTypeFactor(int itemType)
        {
            decimal itemTypeFactor;

            switch (itemType)
            {
                case 1:
                    itemTypeFactor = 1.1M;
                    break;
                case 2:
                    itemTypeFactor = 2.5M;
                    break;
                case 3:
                    itemTypeFactor = 8.43M;
                    break;
                default:
                    return -1;
            }

            return itemTypeFactor;
        }

        private static decimal GetMaterialTypeFactor(int materialType)
        {
            decimal materialTypeFactor;

            switch (materialType)
            {
                case 1:
                    materialTypeFactor = 1M - 0.003M;
                    break;
                case 2:
                    materialTypeFactor = 1M - 0.0012M;
                    break;
                default:
                    return -1;
            }

            return materialTypeFactor;
        }

        private static long CalculateQuantity(int itemCount, int itemWidth, int itemLength, decimal itemTypeFactor, decimal materialTypeFactor)
        {
            decimal quantity = (decimal)itemCount * itemWidth * itemLength * itemTypeFactor / materialTypeFactor;
            return (long)Math.Ceiling(quantity);
        }
    }
}
